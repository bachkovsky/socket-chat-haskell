{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Concurrent

import Control.Exception
import Control.Exception.Base
import Control.Monad (forever, when)
import Network.Socket
import System.IO

main :: IO ()
main = do
  putStrLn "start"
  sock <- socket AF_INET Stream 0
  setSocketOption sock ReuseAddr 1
  bind sock (SockAddrInet 4242 iNADDR_ANY)
  listen sock 2
  chan <- newChan
  _ <- forkIO $ forever $ readChan chan
  mainLoop sock chan 0

type Msg = (Int, String)

mainLoop :: Socket -> Chan Msg -> Int -> IO ()
mainLoop sock chan msgNum = do
  conn <- accept sock
  let newNum = msgNum + 1
  putStrLn $ "new connection accepted, id: " ++ show newNum
  _ <- forkIO (runConn chan msgNum conn)
  mainLoop sock chan $! newNum

runConn :: Chan Msg -> Int -> (Socket, SockAddr) -> IO ()
runConn chan msgNum (sock, _) = do
  let broadcast msg = writeChan chan (msgNum, msg)
  hdl <- socketToHandle sock ReadWriteMode
  hSetBuffering hdl NoBuffering
  hPutStrLn hdl "Hi, what's your name?"
  name <- fmap init (hGetLine hdl)
  putStrLn $ name ++ " entered chat"
  broadcast ("--> " ++ name ++ " entered chat.")
  commLine <- dupChan chan
  -- fork off a thread for reading from the duplicated channel
  reader <-
    forkIO $
    forever $ do
      (nextNum, line) <- readChan commLine
      when (msgNum /= nextNum) $ hPutStrLn hdl line
  handle (\(SomeException _) -> return ()) $
    forever $ do
      line <- fmap init (hGetLine hdl)
      case line
          -- If an exception is caught, send a message and break the loop
            of
        "quit" -> hPutStrLn hdl "Bye!" >> throw (toException FixIOException)
          -- else, continue looping.
        _ -> broadcast (name ++ ": " ++ line)
  -- kill after the loop ends
  killThread reader
  putStrLn $ name ++ " left chat"
  -- make a final broadcast
  broadcast ("<-- " ++ name ++ " left.")
  hClose hdl
